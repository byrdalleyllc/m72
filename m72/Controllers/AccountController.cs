﻿using System;
using System.Security.Claims;
using System.Threading.Tasks;
using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Options;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authorization;
using main_app.Models;

namespace main_app.Controllers
{
	[AllowAnonymous]
	public class AccountController : Controller
	{
		public AppConfig config { get; }
		public IHttpContextAccessor contextAccessor;

		public AccountController(IOptions<AppConfig> config, IHttpContextAccessor contextAccessor)
		{
			this.config = config.Value;
			this.contextAccessor = contextAccessor;
		}

		public async Task<ActionResult> Login(string username, string password)
		{
			if (username == this.config.Config.authUsername && password == this.config.Config.authPassword)
			{
				var request = this.contextAccessor.HttpContext.Request;
				string issuer = string.Format("{0}://{1}", request.Scheme, request.Host);

				var claims = new List<Claim> {
				new Claim(ClaimTypes.Role, "user", ClaimValueTypes.String, issuer)
			};

				var userIdentity = new ClaimsIdentity(claims, "login");
				var userPrincipal = new ClaimsPrincipal(userIdentity);

				await HttpContext.Authentication.SignInAsync("Cookie", userPrincipal);

                return RedirectToAction("Index", this.config.Config.EnabledSite == "HCP" ? "hcp" : "home");
			}
			return View();
		}
	}
}
