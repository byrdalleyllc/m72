module.exports.tasks = {
  imagemin: {
    dist: {
      files: [{
        expand: true,                                               // Enable dynamic expansion
        cwd: '<%= gruntfileConfig.paths.img %>/',                   // Src matches are relative to this path
        src: ['**/*.{svg,png,jpg,gif}'],                            // Actual patterns to match
        dest: '<%= paths.dist %>/<%= gruntfileConfig.paths.img %>'  // Destination path prefix
      }]
    }
  }
};