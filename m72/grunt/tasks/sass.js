module.exports.tasks = {
	sass: {
		default: {
			files: [{
					expand: true,
					src:    [ '**/*.scss' ],
					cwd:    '<%= paths.sass %>',
					dest:   '<%= paths.dist %>/<%= paths.css %>',
					ext:    '.css'
				}
			]
		}
	}
};