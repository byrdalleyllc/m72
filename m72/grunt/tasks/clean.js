module.exports.tasks = {
  clean: {
    css: {
      src: ["<%= paths.css %>"]
    },
    dist: {
      src: ["<%= paths.dist %>"]
    },
  }
};