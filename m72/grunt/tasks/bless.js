module.exports.tasks = {
  bless: {
    css: {
      options: {
        banner: '/* Generates from <%= gruntfileConfig.files.compiledStyleFilename %> */',
        compress: true,
        cleanup: true,
        logCount: 'warn'
      },
      files: {
        '<%= gruntfileConfig.paths.tmp %>/<%= gruntfileConfig.paths.css %>/<%= gruntfileConfig.files.compiledStyleFilenameIE %>': '<%= gruntfileConfig.paths.tmp %>/<%= gruntfileConfig.paths.css %>/<%= gruntfileConfig.files.compiledStyleFilename %>'
      }
    }
  }
};