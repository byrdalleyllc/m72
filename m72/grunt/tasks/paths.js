module.exports.tasks = {
  "img": "images",
  "fonts": "fonts",
  "js": "js",
  "css": "css",
  "sass": "sass",
  "misc": "misc",
  "tmpl": "templates",
  "dist": "dist",
  "docs": "dist/docs",
  "tmp": ".tmp"
}