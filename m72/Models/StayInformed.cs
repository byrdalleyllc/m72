using System;
using System.ComponentModel.DataAnnotations;
using main_app.ValidationAttributes;

namespace main_app.Models
{
    public class StayInformed
    {
        private const string RequireOneFromGroupIsPatientCaregiverSelector = ".is-patient-caregiver";
        private const string RequireOneFromGroupPatientSelector = ".user-type-patient";
        private const string RequireOneFromGroupCaregiverSelector = ".user-type-caregiver";

        public StayInformed()
        {
            States = new StateList();
        }
        
        [Required(ErrorMessage = "Please enter valid email address.")]
        [EmailAddress]
        public string Email{get;set;}

        [Compare("Email", ErrorMessage = "Email address does not match.")]
        public string ConfirmEmail{get;set;}
        
        [Required(ErrorMessage="First name is required.")]
        public string FirstName{get;set;}

        [Required(ErrorMessage="Last name is required.")]
        public string LastName{get;set;}

        [RequireIfTrueValidation("DeliverToHome",ErrorMessage="Address is required.")]
        public string AddressLine1{get;set;}
        public string AddressLine2{get;set;}

        [RequireIfTrueValidation("DeliverToHome",ErrorMessage="ZIP is required.")]
        public string Zip{get;set;}

        [RequireIfTrueValidation("DeliverToHome",ErrorMessage="City is required.")]
        public string City{get;set;}
        [RequireIfTrueValidation("DeliverToHome",ErrorMessage="State is required.")]
        public string State{get;set;}
        
        public bool DeliverToHome{get;set;}

        [Range(typeof(bool), "true", "true", ErrorMessage = "Please opt in to continue.")]
        public bool Terms{get;set;}

        public StateList States{get;set;}

        [Required(ErrorMessage="Please fill at least 1 of these fields")]
        public UserTypeChoice? UserType{get;set;}

        [RequireIfEqualValidation("UserType", UserTypeChoice.Patient, ErrorMessage="Pick choose one of the following.")]
        public PatientStatusChoice? PatientStatus{get;set;}

        
        [RequireIfEqualValidation("UserType", UserTypeChoice.Caregiver, ErrorMessage="Pick choose one of the following.")]
        public CaregiverStatusChoice? CaregiverStatus{get;set;}
    }

    public enum UserTypeChoice
    {
        Patient,
        Caregiver
    }

    public enum PatientStatusChoice
    {
        TakingActimmune,
        CGDWithoutActimmune

    }

    public enum CaregiverStatusChoice
    {
        TakingActimmune,
        CGDWithoutActimmune

    }
}