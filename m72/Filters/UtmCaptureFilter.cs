using System.Linq;
using Microsoft.AspNetCore.Mvc.Filters;
using Microsoft.AspNetCore.Session;

namespace main_app.Filters {
    public class UtmCaptureFilter : ActionFilterAttribute 
    {
        public override void OnActionExecuting(ActionExecutingContext context)
        {
            var utmsource = context.HttpContext.Request.Query["utm_source"].FirstOrDefault();

            if (utmsource != null) {
                context.HttpContext.Session.Set("utm_source", System.Text.Encoding.UTF8.GetBytes(utmsource));
            }

            base.OnActionExecuting(context);
        }
    }
}