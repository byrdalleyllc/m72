$(document).ready(function() {
    $('[data-toggle=modal-verify]').click(function(e) {
        e.preventDefault();

           $('#modal-verify').modal('show');
    });

    $('#modal-verify .btn-primary').click(function() {
        
       if ($('#modal-verify #styled-checkbox-1').prop("checked")){
        var url = $(this).attr('href');  
          e.preventDefault();
          window.open(url, '_blank');
        }
        else{
            return false;
        }
    });
});