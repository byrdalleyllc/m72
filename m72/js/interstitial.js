function showInterstitial(title, body, link) {
    var i = $('#interstitial');

    i.on('show.bs.modal', function(e) {
        var m = $(this);
        m.find('.modal-title').text(title);
        m.find('.modal-body').text(body);
        m.find('.btn-primary').attr('target', '_blank').attr('href', link).click();
    });

    i.modal();
}

function goInternal(link) {
    var title = 'You are leaving the CGD Connections™ website.',
        body = 'You are about to go to a website that is sponsored by Horizon Pharma. Would you like to continue?';

    showInterstitial(title, body, link);
}

function goExternal(link) {
    var title = 'You are leaving the CGD Connections™ website.',
        body = 'You will be going to an external website operated by an independent third party. Your activities at the external website will be managed by their policies and practices. By clicking “Continue,” you will go to the external website.';

    showInterstitial(title, body, link);
}

var linkMap = {
    external: goExternal,
    internal: goInternal
};

$(document).ready(function() {
    /*$('[data-toggle=modal]').click(function(e) {
        e.preventDefault();

        var target = $(e.target);
        if (!target.is('a')) {
            target = target.closest('a');
        }

        var link = target.attr('href'),
            type = target.attr('data-link');

            goInternal(link);
    });

    $('#interstitial .btn-primary').click(function() {
        $('#interstitial').modal('hide');
    });*/
});