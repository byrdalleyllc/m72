function showImage(link) {
    var i = $('#expandable-image');

    i.on('show.bs.modal', function(e) {
        var m = $(this);
        m.find('.modal-main-image').attr('src', link);
    });

    i.modal();
}

$(document).ready(function() {
    $('[data-expandable=1]').click(function(e) {
        e.preventDefault();

        var target = $(e.target).eq(0);
        if (!target.is('img')) {
            target = target.closest('[data-expandable=1]').find('img');
        }

        var link = target.attr('src');

        showImage(link);
    });

    $('#expandable-image .btn-primary').click(function() {
        $('#expandable-image').modal('hide');
    });
});