var search = {
    initialize: function() {
        if ($(window).width() > 1024) {
            var nav = $('.layout--search-nav'),
                trigger = $('.navigation .search');

            nav.css('left', trigger.offset().left - nav.width() + 80);

            trigger.click(function() {
                nav.toggle();
            })
        }
    }
};

//search.initialize();

var menus = {
    main: null,
    search: null,
    active: null,
    initialize: function() {
        this.main = _.extend({}, menu);
        this.main.initialize('.layout--main-nav', '.navigation', '.mobile-triggers > .nav');

        this.search = _.extend({}, menu);
        this.search.initialize('.layout--main-nav', '.layout--search-nav', '.search');

        var triggers = $('.mobile-triggers');

        triggers.find('> .nav').click($.proxy(this.showMain, this));
        triggers.find('.search').click($.proxy(this.showSearch, this));

        $(window).on('adjustWidth', $.proxy(this.hideAll, this));

        var that = this;
        $(window).on('adjustWidth', function() {that.main.onAdjustWidth();});
    },
    toggle: function(show, hide) {
        if (this.active != show) {
            this[hide].hide();
            this[show].show();

            this.active = show;
        } else {
            this.hideAll();
        }
    },
    hideAll: function() {
        this.main.hide();
        this.search.hide();
        this.active = null;
    },
    showMain: function() {
        this.toggle('main','search');
    },
    showSearch: function() {
        this.toggle('search','main');
    }
};

var menu = {
    menu: null,
    screen: null,
    trigger: null,
    initialize: function(containerSelector, menuSelector, triggerSelector) {
        var container = $(containerSelector);
        this.menu = container.find(menuSelector);
        this.screen = container.find('.screen');
        this.trigger = container.find(triggerSelector);

        this.updateNavHeight();

        this.initializeExpand();
        this.resetSubNavs();
    },
    initializeExpand: function() {
        this.resetSubNavs();

        var isMobile = this.isMobile;
        this.menu.find('> li').hover(
            function() {if (!isMobile()) $(this).addClass('hover');},
            function() {if (!isMobile()) $(this).removeClass('hover');}
        );

        if (!this.isMobile()) {
            this.menu.find('.trigger').remove();
        } else {
            this.menu.find('.expand').append('<span class="trigger"></span>');
            var expandIcon = this.menu.find('.expand .trigger');
            expandIcon.click($.proxy(this.toggleSubNav,this));
        }
    },
    resetSubNavs: function() {
        var subNavs = this.menu.find('.sub-nav');
        if (this.isMobile()) {
            subNavs.hide();
        } else {
            subNavs.removeAttr('style');
        }

        this.menu.find('.expand').addClass('inactive');
    },
    isMobile: function() {
        return $(window).width() <= vars.mobile.width;
    },
    show: function() {
        this.resetSubNavs();

        this.updateOther(true);

        this.menu.show();
        this.trigger.addClass('open');

        $(window).trigger('showNav');
    },
    hide: function() {
        this.updateOther(false);

        this.menu.hide();
        this.trigger.removeClass('open');

        $(window).trigger('hideNav');
    },
    toggleSubNav: function(e) {
        if (this.isMobile()) {
            e.preventDefault();
            this.resetSubNavs();
            this.updateNavHeight();

            var t = $(e.target);
            t.closest('a').toggleClass('inactive');
            t.closest('li').find('.sub-nav').toggle(100);
        }
    },
    updateNavHeight: function() {
        if (this.isMobile()) {
            //this.menu.height($(window).height());
        }
    },
    updateOther: function(forShow) {
    },
    onAdjustWidth: function() {
        this.initializeExpand();

        if (!this.isMobile()) {
            this.show();
            this.menu.css('display', 'flex');
        } else {
            this.menu.css('display', 'block').hide(); // set default display type before hiding
            this.hide();
        }

        $(window).trigger('menuInitialized');
    }
};

menus.initialize();