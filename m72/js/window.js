var browserWindow = {
    initialize: function() {
        var initialWidth = $(window).width();

        $(window).resize(function() {
            if (Math.abs($(window).width() - initialWidth) > 50) {
                $(window).trigger('adjustWidth');

                initialWidth = $(window).width();
            }
        });
    }
}

browserWindow.initialize();