var scrollToElement = function(selector, time) {
    $('html, body').animate({scrollTop: $(selector).offset().top}, time);
}

$('.to-page-top').click(function() {
    scrollToElement('html', 1000);
});
