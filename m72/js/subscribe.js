$('[name=DeliverToHome]').change(function(e) {
    var target = $(e.target),
        fields = $('.subscribe-snail-mail');

    if (target.val() == "true") {
        fields.show();
    } else {
        fields.hide();
    }
})